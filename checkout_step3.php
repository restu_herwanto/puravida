<?php
$page = "shop";
include('inc_header.php');

?>
<div class="clear"></div>
<!-- middle -->
<section>
	<div id="middle" class="checkout_s">
		<div class="wrapper_content_page">	
			<div class="content checkout_s_w">
				<h1>Checkout</h1>
				<div class="wrap_checkout">
				<div class="row">
					<ul class="menu_checkout_step">
						<li>
							<span class="lf step_1"></span>
							<span class="rh">
								<h4>Step 1</h4>
								<h3>sign in</h3>
							</span>
						</li>
						<li>
							<span class="space_dot"></span>
						</li>
						<li>
							<span class="lf step_2"></span>
							<span class="rh">
								<h4>Step 2</h4>
								<h3>SHIPPING</h3>
							</span>
						</li>
						<li>
							<span class="space_dot"></span>
						</li>
						<li>
							<span class="lf step_3 active"></span>
							<span class="rh active">
								<h4>Step 3</h4>
								<h3>payment</h3>
							</span>
						</li>
						<li>
							<span class="space_dot"></span>
						</li>
						<li>
							<span class="lf step_4"></span>
							<span class="rh">
								<h4>Step 4</h4>
								<h3>Review</h3>
							</span>
						</li>
					</ul>
				</div>
				<div class="errorSummary">
					<ul>
						<li>- Please enter your email</li>
						<li>- You must agree to our terms and conditions before continuing</li>
					</ul>
				</div>
				<div class="row form_payment">
					<form>
					<div class="w_form_payment">
					<div class="left form_paymen_l">
						<div class="w_form_signin">
							<h3 class="title_form">I Prefer to Pay By</h3>
								<div class="row">
									<input id="radio_cc" class="radio_pm" type="radio" name="method" value="1" data-accordion="" checked="checked"><label for="radio_cc">Credit Card</label><br>
									<input id="radio_bt" class="radio_pm" type="radio" name="method" value="2" data-accordion="for_bt"><label class="last" for="radio_bt">Bank Transfer</label>
									<div class="for_bt accordion_payment">
										<p> All transfer payments must be made within 48 hours otherwise order will be cancelled. To confirm scheduled home delivery, payments must be made 24 hours before delivery date otherwise order will be cancelled.<br><br>Transfers can be made to: </p>
										<div class="detail_for_radio_bt">
											
											<div class="list_bank">
												<span><img src="images/material/logo_bca.jpg"/></span> 
												<span>
													<h4>Florence Vanessa Budihardja</h4>
													<h5>BCA   7310311213</h5>
												</span>
											</div>
											
											<div class="list_bank">
												<span><img src="images/material/logo_mandiri.jpg"/></span> 
												<span>
													<h4>Krizia Darius Liauw</h4>
													<h5>MANDIRI   1020020142243</h5>
												</span>
											</div>
											
										</div>
									</div>
									
								</div>
						<div class="clear"></div>
						</div>
						<div class="clear"></div>
						<div class="w_form_billing">
							<h3 class="title_form">Billing Address</h3>
							<div class="row">
								<input id="checkbox_billing" type="checkbox" name="checkbox" value="1"/>
								<label for="checkbox_billing">Same as delivery address</label>
							</div>
							<div class="row">
								<input type="text" name="name" placeholder="Name *"/>
								<input type="text" name="phone" placeholder="Phone *"/>
								<textarea placeholder="Address"></textarea>
								<select class="select_province">
									<option value="" disabled selected>Select Province *</option>
									<option value="">option1</option>
									<option value="">option2</option>
								</select>
								<select class="select_city">
									<option value="" disabled selected>Select City *</option>
									<option value="">option1</option>
									<option value="">option2</option>
								</select>
							</div>
						</div>
					
					</div>
					<div class="left form_paymen_r">
						<div class="w_form_order_summary">
							<h3 class="title_form">Order Summary</h3>
							<div class="table_order_summary">
								<table class="order_Summary_w">
									<tr class="os_title">
										<td class="os_products">Products</td>
										<td class="os_qty">Qty</td>
										<td class="os_price">Price</td>
									</tr>
									<tr class="os_list_order">
										<td>Chucky Monkey - Medium Size</td>
										<td>2</td>
										<td>140.000,-</td>
									</tr>
									<tr class="os_list_order">
										<td>Kakao Chia - Small Size</td>
										<td>4</td>
										<td>220.000,-</td>
									</tr>
									<tr class="os_detail a">
										<td>Sub Total</td>
										<td></td>
										<td>360.000,-</td>
									</tr>
									<tr class="os_detail b">
										<td>Delivery Fee</td>
										<td></td>
										<td>15.000,-</td>
									</tr>
									<tr class="os_detail c">
										<td>Voucher</td>
										<td></td>
										<td>( 100.000,- )</td>
									</tr>
									
									<tr class="os_promocode"> 
										<td colspan="3">
											<input type="text" name="promocode" placeholder="Promo Code"/>
											<button>APPLY</button> 
										</td>
									</tr>
									<tr class="os_total"> 
										<td>TOTAL</td>
										<td></td>
										<td>275.000,-</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
					<div class="clear"></div>
					</div>
					<div class="clear"></div>
					<div class="row_btn_act">
						<div class="wrap_row_btn_act">
							<button>back</button>
							<input type="submit" value="Continue"/>
						</div>
					</div>
					</form>
					
				</div>
				
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	
</section>
<!-- end of middle -->
<div class="clear"></div>
<?php include('inc_footer.php');?>
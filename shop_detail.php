<?php
$page = "shop";
include('inc_header.php');

?>
<div class="clear"></div>
<!-- middle -->
<section>
	<div id="middle" class="shopdetail">
		<div class="banner_content" style="background: url(images/slider/banner_shopdetail.jpg)no-repeat center center ;background-size:cover;">
			
		</div>
		<div class="content shopdetail_product">
			<div class="wrapper_content_page">
				<div class="product_detail_lr">
					<div class="product_img left">
						<img src="images/content/product_detail.jpg"/>
					</div>
					<div class="product_detail left">
					<div class="product_detail_wrap">
						<h2>NOCHE Overnight Oats</h2>
						<h1>Strawberry CheeseCake</h1>
						<div class="row size_price">
							<div class="size left">
								<span class="size_d">S</span>
								<span class="size_d active">M</span>
								<span class="label">Medium</span>
							</div>
							<div class="price left">
								<span class="price_value"><span class="label_p">Rp.</span> 70.000,-</span>
							</div>
						</div>
						<div class="row list_des">
							<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. </p>
							<ul>
								<li>Old fashioned rolled oats</li>
								<li>Organic strawberries</li>
								<li>Plain nonfat yogurt</li>
								<li>Homemade almond milk</li>
								<li>Vanilla whey protein</li>
								<li>Cottage cheese</li>
								<li>Dates</li>
							</ul>
						</div>
						<div class="row list_det">
							<ul>
								<li>
									<span class="list_det_img">
										<img src="images/material/serving_size_icon_det.jpg"/>
									</span>
									<span class="list_det_text">
										<h5>Serving size:</h5>
										<h6><span class="list_det_text_size"></span>280 gr<span class="list_det_text_size"></span>160 gr</h6>
									</span>
								</li>
								<li>
									<span class="list_det_img">
										<img src="images/material/in_fridge_icon_det.jpg"/>
									</span>
									<span class="list_det_text">
										<h5>In Fridge</h5>
										<h6>Exp 3-4 days</h6>
									</span>
								</li>
								<li>
									<span class="list_det_img">
										<img src="images/material/in_freezer_icon_det.jpg"/>
									</span>
									<span class="list_det_text">
										<h5>In Feezer</h5>
										<h6>Exp 2 weeks</h6>
									</span>
								</li> 
							</ul>
						</div>
					</div>	
					</div>
					<div class="clear"></div>
				</div>
				
			</div>
			<div class="clear"></div>
				<div class="bar_detail">
					<div class="wrapper_content_page">
						<div class="bar_detail_lr">
						<div class="label_l left">
							<span>Order <a href="">NOW!</a></span>
						</div>
						<div class="form_r left">
							<form>
								<div class="left serving_size_w">
									<span class="label">serving Size</span>
									<select class="serving_size">
										<option value="">Small</option>
										<option value="">Large</option>
									</select>
								</div>
								<div class="left delivery_date_w">
									<span class="label">Delivery Date</span>
									<input class="date_bar" type="text" id="datepicker" placeholder="dd-mm-yyyy"/>
								</div>
								<div class="left qty_w">
									<span class="label">qty</span>
									<select class="qty_val">
										<option value="">Small</option>
										<option value="">Large</option>
									</select>
								</div>
								<div class="left btn_submit_w">
									<input type="submit" value="add to cart"/>
								</div>
								<div class="clear"></div>
								<p class="tagline_label_val">* Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
							</form>
						</div>
						<div class="clear"></div>
						</div>
					</div>
					
				</div>
			<div class="clear"></div>
		</div>
	</div>
	<div id="middle" class="recommended_items">
		<div class="wrapper_recommended_items wrapper">
			<h2 class="title_page">You May Also Like</h2>
			<div class="wrapper_control_carousel">
				<div class="nav_left" id="slider-next"></div>
				<div class="label_center">View All</div>
				<div class="nav_right" id="slider-prev"></div>
			</div>
			<div class="clear"></div>
			<div class="slider_recommended_items">
				<div class="slide">
					<div class="wrap_slide" current-index="data1" data-index="1">
						<img src="images/content/best_seller1.jpg"/>
						<h4 class="title_slide_base">Chunky Monkey</h4>
						<h5>Noche Overnight Oat</h5>
						<h6>70.000,-</h6>
						<span class="wrap_product_size">
							<span class="product_size active">S</span>
							<span class="product_size">M</span>
						</span>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" current-index="data2" data-index="2">
						<img src="images/content/best_seller2.jpg"/>
						<h4 class="title_slide_base">Kakao Chia</h4>
						<h5>Noche Overnight Oat</h5>
						<h6>70.000,-</h6>
						<span class="wrap_product_size">
							<span class="product_size active">S</span>
							<span class="product_size">M</span>
						</span>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" current-index="data3" data-index="3">
						<img src="images/content/best_seller3.jpg"/>
						<h4 class="title_slide_base">Purely Granola</h4>
						<h5>Noche Overnight Oat</h5>
						<h6>70.000,-</h6>
						<span class="wrap_product_size">
							<span class="product_size active">S</span>
							<span class="product_size">M</span>
						</span>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" current-index="data4" data-index="4">
						<img src="images/content/best_seller4.jpg"/>
						<h4 class="title_slide_base">Organic Honey</h4>
						<h5>Noche Overnight Oat</h5>
						<h6>70.000,-</h6>
						<span class="wrap_product_size">
							<span class="product_size active">S</span>
							<span class="product_size">M</span>
						</span>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" current-index="data5" data-index="5">
						<img src="images/content/best_seller1.jpg"/>
						<h4 class="title_slide_base">Chunky Monkey</h4>
						<h5>Noche Overnight Oat</h5>
						<h6>70.000,-</h6>
						<span class="wrap_product_size">
							<span class="product_size active">S</span>
							<span class="product_size">M</span>
						</span>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" current-index="data6" data-index="6">
						<img src="images/content/best_seller2.jpg"/>
						<h4 class="title_slide_base">Kakao Chia</h4>
						<h5>Noche Overnight Oat</h5>
						<h6>70.000,-</h6>
						<span class="wrap_product_size">
							<span class="product_size active">S</span>
							<span class="product_size">M</span>
						</span>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" current-index="data7" data-index="7">
						<img src="images/content/best_seller3.jpg"/>
						<h4 class="title_slide_base">Purely Granola</h4>
						<h5>Noche Overnight Oat</h5>
						<h6>70.000,-</h6>
						<span class="wrap_product_size">
							<span class="product_size active">S</span>
							<span class="product_size">M</span>
						</span>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" current-index="data8" data-index="8">
						<img src="images/content/best_seller4.jpg"/>
						<h4 class="title_slide_base">Organic Honey</h4>
						<h5>Noche Overnight Oat</h5>
						<h6>70.000,-</h6>
						<span class="wrap_product_size">
							<span class="product_size active">S</span>
							<span class="product_size">M</span>
						</span>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" current-index="data9" data-index="9">
						<img src="images/content/best_seller1.jpg"/>
						<h4 class="title_slide_base">Chunky Monkey</h4>
						<h5>Noche Overnight Oat</h5>
						<h6>70.000,-</h6>
						<span class="wrap_product_size">
							<span class="product_size active">S</span>
							<span class="product_size">M</span>
						</span>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" current-index="data10" data-index="10">
						<img src="images/content/best_seller2.jpg"/>
						<h4 class="title_slide_base">Kakao Chia</h4>
						<h5>Noche Overnight Oat</h5>
						<h6>70.000,-</h6>
						<span class="wrap_product_size">
							<span class="product_size active">S</span>
							<span class="product_size">M</span>
						</span>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" current-index="data11" data-index="11">
						<img src="images/content/best_seller3.jpg"/>
						<h4 class="title_slide_base">Purely Granola</h4>
						<h5>Noche Overnight Oat</h5>
						<h6>70.000,-</h6>
						<span class="wrap_product_size">
							<span class="product_size active">S</span>
							<span class="product_size">M</span>
						</span>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" current-index="data12" data-index="12">
						<img src="images/content/best_seller4.jpg"/>
						<h4 class="title_slide_base">Organic Honey</h4>
						<h5>Noche Overnight Oat</h5>
						<h6>70.000,-</h6>
						<span class="wrap_product_size">
							<span class="product_size active">S</span>
							<span class="product_size">M</span>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end of middle -->
<div class="clear"></div>
<?php include('inc_footer.php');?>
    	<!--Footer -->
        <footer>
			<div class="footer">
				<div class="wrapper_footer">
					<ul class="footer_menu_left">
						<li><a href="">Home</a></li>
						<li><a href="">Shop</a></li>
						<li><a href="">About</a></li>
						<li><a href="">FAQ</a></li>
						<li><a href="">Thoughts</a></li>
						<li><a href="">Contact</a></li>
					</ul>
					<ul class="footer_menu_right">
						<li><a href="">Terms & Privacy</a></li>
						<li><a href="">For Media</a></li>
						<li><a href="">Career</a></li>
					</ul>
					<div class="clear"></div>
					<div class="wrapper_copyright">
						<div class="logo_socmed_copyright">
							<div class="logo_socmed_copyright_position">
								<a href=""><span class="wrap_icon_copyright instagram_icon_f"></span></a>
								<a href=""><span class="wrap_icon_copyright facebook_icon_f"></span></a>
								<a href=""><span class="wrap_icon_copyright twitter_icon_f"></span></a>
								<a href=""><span class="wrap_icon_copyright message_icon_f"></span></a>
							</div>
						</div>
						<p class="text_copyright">
						Copyright © 2015<br>PURAVIDA
						</p>
					</div>
				</div>
				
				<div class="clear"></div>
			</div>
			<div class="subscribe_newsletter">
			<div class="wrap_s_newsletter">
				<div class="before_h"><span>Subscribe</span> to Our Newsletter</div>
				<div class="after_h"><span>Subscribe</span> to Our Newsletter</div>
			</div>
			</div>
			<div class="subscribe_newsletter_form">
				<div class="wrapper wrap_box_newsletter">
					<h3><span>Subscribe</span> to Our Newsletter</h3>
					<h4>Sign up and get on the down low on our latest updates, promotions and healthy living tips.</h4>
					<div class="wrap_form_newsletter">
						<form>
							<input type="text" placeholder="Email address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email address'"/>
							<input type="submit" value="SUBMIT"/>
						</form>
					</div>
					<p class="tagline_newsletter">We respect your privacy.</p>
				</div>
				<div class="close_icon_nav"></div>
			</div>
			<div class="clear"></div>
		</footer>
        <!--end of Footer -->
		<div class="overlay_popup">
			<div class="wrapper_popup">
				<div class="wrapper_popup_title">Hello There!<span class="close_pop"></span></div>
				<div class="wrapper_form_pop">
					<div class="sign_up">
						<h3>Sign Up</h3>
						<h4>First time here? Please sign up.</h4>
						<p>Register your e-mail to get special deals By creating an account with our store, you will be able to move through the checkout process faster, get special deals and so much more.</p>
						<a href=""><div class="btn_signup">Create my account</div></a>
					</div>
					<div class="sign_in">
						<div class="sign_in_wrap">
							<h3>Sign In</h3>
							<h4>Good to have you back! Please sign in.</h4>
							<form>
								<input type="text" name="email" placeholder="Email"/>
								<input type="text" name="password" placeholder="Password"/>
								<div class="wrap_remember_forgot">
									<span class="remember_me">
										 <input id="checkbox_reg" type="checkbox" name="checkbox" value="1"/><label for="checkbox_reg">Remember Me</label>
									</span>
									<span class="forgot_password">
										Forgot <a href="">Password?</a>
									</span>
								</div>
								<div class="row_btn">
									<input type="submit" value="Sign in" class="signin"/>
									<span class="space_or">Or</span>
									<input type="submit" value="Sign in with facebook" class="signin_with_fb"/>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
    </body>
</html>
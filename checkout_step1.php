<?php
$page = "shop";
include('inc_header.php');

?>
<div class="clear"></div>
<!-- middle -->
<section>
	<div id="middle" class="checkout_s">
		<div class="wrapper_content_page">	
			<div class="content checkout_s_w">
				<h1>Checkout</h1>
				<div class="wrap_checkout">
				<div class="row">
					<ul class="menu_checkout_step">
						<li>
							<span class="lf step_1 active"></span>
							<span class="rh active">
								<h4>Step 1</h4>
								<h3>sign in</h3>
							</span>
						</li>
						<li>
							<span class="space_dot"></span>
						</li>
						<li>
							<span class="lf step_2"></span>
							<span class="rh">
								<h4>Step 2</h4>
								<h3>SHIPPING</h3>
							</span>
						</li>
						<li>
							<span class="space_dot"></span>
						</li>
						<li>
							<span class="lf step_3"></span>
							<span class="rh">
								<h4>Step 3</h4>
								<h3>payment</h3>
							</span>
						</li>
						<li>
							<span class="space_dot"></span>
						</li>
						<li>
							<span class="lf step_4"></span>
							<span class="rh">
								<h4>Step 4</h4>
								<h3>Review</h3>
							</span>
						</li>
					</ul>
				</div>
				<div class="errorSummary">
					<ul>
						<li>- Please enter your email</li>
						<li>- You must agree to our terms and conditions before continuing</li>
					</ul>
				</div>
				<div class="row form_signin_checkout">
					<div class="left form_signin">
						<div class="w_form_signin">
							<h3 class="title_form">Sign In</h3>
							<form>
								<div class="row">
									<input type="text" name="email" placeholder="Email"/>
									<input type="text" name="password" placeholder="Password"/>
								</div>
								<div class="row row_remember">
									<span class="remember_me">
										<input id="checkbox_signin" type="checkbox" name="checkbox" value="1"/>
										<label for="checkbox_signin">Remember Me</label>
									</span>
									<span class="forgot_password">
										Forgot <a href="">Password?</a>
									</span>
								</div>
								<div class="row_btn">
									<input type="submit" value="Sign in" class="signin"/>
									<span class="space_or">Or</span>
									<input type="submit" value="Sign in with facebook" class="signin_with_fb"/>
								</div>
							</form>
						</div>
						<div class="clear"></div>
						<div class="w_new_customer">
							<h3>New Customer?</h3>
							<p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>
							<button class="btn_create_account">Create MY account</button>
						</div>
					</div>
					<div class="left form_guest">
						<div class="w_form_guest">
							<h3 class="title_form">Continue as Guest</h3>
							<form>
								<div class="row">
									<input type="text" name="name" placeholder="Name"/>
									<input type="text" name="email" placeholder="Email *"/>
									<input type="text" name="re-email" placeholder="Re-type Email *"/>
									<input type="text" name="phone" placeholder="Phone *"/>
								</div>
								<div class="row">
									<input id="checkbox_guest" type="checkbox" name="checkbox" value="1"/>
									<label for="checkbox_guest" class="label_guest_check">I agree to the <a href="">terms and privacy policy</a>, and that Puravida will process my personal data only for purposes regarding fulfillment of an order pursuant to the rules provided in Puravida Security Policy.</label>
								</div>
								<div class="row">
									<a href="checkout_step2.php"><input type="submit" value="submit"/></a>
								</div>
							</form>
						</div>
					</div>
				</div>
				
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	
</section>
<!-- end of middle -->
<div class="clear"></div>
<?php include('inc_footer.php');?>
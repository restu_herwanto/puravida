<?php
$page = "shop";
include('inc_header.php');

?>
<div class="clear"></div>
<!-- middle -->
<section>
	<div id="middle" class="shopping_cart">
		<div class="wrapper_content_page">	
			<div class="content shopping_cart_w">
				<h1>Shopping Cart</h1>
				<div class="table_sc">
					<table class="cart_w">
						<!--cart-->
						<tr class="tr_header">
							<td colspan="5">
								<span class="del_date_wrap">
									<span class="del_date">Delivery Date :</span>
									<span class="del_date_value">February 13, 2015</span>
								</span>
								<span class="act_cart_wrap">
									<span class="act_update_cart"><a href="">Update Cart</a></span>
									<span class="act_remove_all"><a href="">remove All</a></span>
								</span>
							</td>
						</tr>
						
						<tr class="tr_title">
							<td class="td_product">Product</td>
							<td class="td_price">price</td>
							<td class="td_qty">qty</td>
							<td class="td_total">total</td>
							<td class="td_del"> </td>
						</tr>
						
						<tr class="tr_list_product">
							<td class="td_product_des">
								<span class="imgleft">
									<img src="images/content/cart_product_small.jpg"/>
								</span>
								<span class="desleft">
									<h4>Chunky Monkey</h4>
									<h5>Noche Overnight Oat - Medium Size</h5>
								</span>
							</td>
							<td class="td_price_val">70.000,-</td>
							<td class="td_qty_val">
								<span class="qty_wrap">
									<input type="text" value="1" class="quantity_val" readonly/>
									<button class="plus"></button>
									<button class="min"></button>
								</span>
							</td>
							<td class="td_total_val">70.000,-</td>
							<td class="td_del_val">
								<span class="btn_del_cart"></span>
							</td>
						</tr>
						
						<tr class="tr_list_product">
							<td class="td_product_des">
								<span class="imgleft">
									<img src="images/content/cart_product_small.jpg"/>
								</span>
								<span class="desleft">
									<h4>Chunky Monkey</h4>
									<h5>Noche Overnight Oat - Medium Size</h5>
								</span>
							</td>
							<td class="td_price_val">70.000,-</td>
							<td class="td_qty_val">
								<span class="qty_wrap">
									<input type="text" value="1" class="quantity_val" readonly/>
									<button class="plus"></button>
									<button class="min"></button>
								</span>
							</td>
							<td class="td_total_val">70.000,-</td>
							<td class="td_del_val">
								<span class="btn_del_cart"></span>
							</td>
						</tr>
					
						
						<tr class="total_price_val">
							<td colspan="5">
								<span>180.000,-</span>
							</td>
						</tr>
						<!--end_cart-->
						<!--cart-->
						<tr class="tr_header">
							<td colspan="5">
								<span class="del_date_wrap">
									<span class="del_date">Delivery Date :</span>
									<span class="del_date_value">February 13, 2015</span>
								</span>
								<span class="act_cart_wrap">
									<span class="act_update_cart"><a href="">Update Cart</a></span>
									<span class="act_remove_all"><a href="">remove All</a></span>
								</span>
							</td>
						</tr>
						<tr class="tr_title">
							<td class="td_product">Product</td>
							<td class="td_price">price</td>
							<td class="td_qty">qty</td>
							<td class="td_total">total</td>
							<td class="td_del"> </td>
						</tr>
						
						<tr class="tr_list_product">
							<td class="td_product_des">
								<span class="imgleft">
									<img src="images/content/cart_product_small.jpg"/>
								</span>
								<span class="desleft">
									<h4>Chunky Monkey</h4>
									<h5>Noche Overnight Oat - Medium Size</h5>
								</span>
							</td>
							<td class="td_price_val">70.000,-</td>
							<td class="td_qty_val">
								<span class="qty_wrap">
									<input type="text" value="1" class="quantity_val" readonly/>
									<button class="plus"></button>
									<button class="min"></button>
								</span>
							</td>
							<td class="td_total_val">70.000,-</td>
							<td class="td_del_val">
								<span class="btn_del_cart"></span>
							</td>
						</tr>
						
						<tr class="tr_list_product">
							<td class="td_product_des">
								<span class="imgleft">
									<img src="images/content/cart_product_small.jpg"/>
								</span>
								<span class="desleft">
									<h4>Chunky Monkey</h4>
									<h5>Noche Overnight Oat - Medium Size</h5>
								</span>
							</td>
							<td class="td_price_val">70.000,-</td>
							<td class="td_qty_val">
								<span class="qty_wrap">
									<input type="text" value="1" class="quantity_val" readonly/>
									<button class="plus"></button>
									<button class="min"></button>
								</span>
							</td>
							<td class="td_total_val">70.000,-</td>
							<td class="td_del_val">
								<span class="btn_del_cart"></span>
							</td>
						</tr>
					
						
						<tr class="total_price_val">
							<td colspan="5">
								<span>180.000,-</span>
							</td>
						</tr>
						<!--end_cart-->
						<tr class="subtotal_price_val">
							<td colspan="5">
								<span class="subtotal_price_val_w">
									<span class="subtotal_pv_label">sub Total</span>
									<span class="subtotal_pv_val">360.000,-</span>
								</span>
							</td>
						</tr>
					</table>
					<div class="btn_action_direct">
						<button class="btn_continue_shopping">Continue Shopping</button>
						<button class="btn_prooceed_to_checkout">proceed to checkout</button>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	
</section>
<!-- end of middle -->
<div class="clear"></div>
<?php include('inc_footer.php');?>
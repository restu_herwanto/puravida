<?php
$page = "account";
include('inc_header.php');

?>
<div class="clear"></div>
<!-- middle -->
<section>
	<div id="middle" class="account_s">
		<div class="wrapper_content_page">	
			<div class="content account_s_w">
				<div class="account_s_l">
					<div class="sidebar_menu">
						<h4>My Account</h4>
						<ul>
							<li><a href="" class="">dashboard</a></li>
							<li><a href="" class="">Personal DetailS</a></li>
							<li><a href="" class="">Change Password</a></li>
							<li><a href="" class="active">Delivery Info</a></li>
							<li><a href="" class="">Order HISTORY</a></li>
							<li><a href="" class="">Payment Confirmation</a></li>
						</ul>
					</div>
				</div>
				<div class="account_s_r">
					<div class="account_content_r">
						<h1>Delivery Info</h1>
						<div class="account_delivery_info first">
							<div class="ad_l">
								<div class="ad_wrapper">
									<div class="shipping_address_icon"></div>
								</div>
							</div>
							<div class="ad_r">
								<div class="ad_wrapper">
									<h5>shipping Address</h5>
									<button>NEW ADDRESS</button>
									<div class="ad_wrapper_inner">
										<ul>
											<?php for($a = 1; $a <=4; $a++){ ?>
											<li>
												<h6>My Address 1</h6>
												<p>
													<b>Mr. Lorem Ipsum</b><br>
													Jl. Bandana loaram No. 78<br>
													RT. 02 RW. 12<br>
													Tangerang - Banten<br>
													08150985465
												</p>
												<a href="">change shipping</a>
											</li>
											<?php } ?>
										</ul>
									</div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div class="account_delivery_info">
							<div class="ad_l">
								<div class="ad_wrapper">
									<div class="billing_address_icon"></div>
								</div>
							</div>
							<div class="ad_r">
								<div class="ad_wrapper">
									<h5>billing Address</h5>
									<button>NEW ADDRESS</button>
									<div class="ad_wrapper_inner">
										<ul>
											<?php for($a = 1; $a <=4; $a++){ ?>
											<li>
												<h6>My Address 1</h6>
												<p>
													<b>Mr. Lorem Ipsum</b><br>
													Jl. Bandana loaram No. 78<br>
													RT. 02 RW. 12<br>
													Tangerang - Banten<br>
													08150985465
												</p>
												<a href="">change billing</a>
											</li>
											<?php } ?>
										</ul>
									</div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	
</section>
<!-- end of middle -->
<div class="clear"></div>
<?php include('inc_footer.php');?>
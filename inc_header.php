<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>WEBARQ - Static Website</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="icon" href="favicon.ico"><!--Style-->
	
	<link href="css/jquery-ui.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/grid_style.css" />
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css" media="screen and (min-width:1024px)" />
	
	<script src="js/vendor/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-ui.js"></script><!--js-->
	<script src="js/TweenMax.min.js"></script>
	<script src="js/wow.min.js"></script>
	
	
	<script src="js/jquery_static_socmed.js"></script>
	<script src="js/jquery_function.js"></script>
	<script src="js/js_run.js"></script>
	
	
	<!--<link rel="stylesheet" href="css/style1.css"/>
	<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>-->
	
	<link href="css/jquery.bxslider.css" rel="stylesheet" /><!--bxslider-->
	<script src="js/jquery.bxslider.min.js"></script>
	 
</head>
<script>
	 new WOW().init();
	  wow = new WOW(
		{ boxClass:     'anime_rh',
		  animateClass: 'animated',
		  offset:       0, 
		  mobile:       true,
		  live:         true 
		}
	  )
	  wow.init();
	</script>
	<script>
	$(document).ready(function() {
	  $('a[href*=#]').each(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
	    && location.hostname == this.hostname
	    && this.hash.replace(/#/,'') ) {
	      var $targetId = $(this.hash), $targetAnchor = $('[name=' + this.hash.slice(1) +']');
	      var $target = $targetId.length ? $targetId : $targetAnchor.length ? $targetAnchor : false;
	       if ($target) {
	         var targetOffset = $target.offset().top;


	         $(this).click(function() {
				$("#nav li a").removeClass("active");
				$(this).addClass('active');
	           $('html, body').animate({scrollTop: targetOffset}, 1000);
	           return false;
	         });
	      }
	    }
	  });

	});


	</script>
<body>
 

<!-- Add your site or application content here --> 

<!-- header -->
<header class="<?php echo ($page !== "home") ? "header_content" : "" ?>">
	<div class="header <?php echo ($page !== "home") ? "header_content" : "" ?>">
		<div class="sign_in" id="pop_signin">
			<div class="img_signin"></div>
			<div class="label italic">Sign In</div>
		</div>
		<div class="wrapper <?php echo ($page !== "home") ? "wrapper_content" : "" ?>">
			<?php include "inc_menu.php"; ?>
		</div>
		<div class="cart_home">
			<div class="img_cart_home"></div>
			<div class="cart_count">2</div>
			<div class="label italic">Basket</div>
		</div>
	</div>
</header>
<!-- end of header -->
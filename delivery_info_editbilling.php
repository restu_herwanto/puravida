<?php
$page = "account";
include('inc_header.php');

?>
<div class="clear"></div>
<!-- middle -->
<section>
	<div id="middle" class="account_s">
		<div class="wrapper_content_page">	
			<div class="content account_s_w">
				<div class="account_s_l">
					<div class="sidebar_menu">
						<h4>My Account</h4>
						<ul>
							<li><a href="" class="">dashboard</a></li>
							<li><a href="" class="">Personal DetailS</a></li>
							<li><a href="" class="">Change Password</a></li>
							<li><a href="" class="active">Delivery Info</a></li>
							<li><a href="" class="">Order HISTORY</a></li>
							<li><a href="" class="">Payment Confirmation</a></li>
						</ul>
					</div>
				</div>
				<div class="account_s_r">
					<div class="account_content_r">
						<h1>Edit Billing Address</h1>
						<div class="inner_content">
							<div class="form_billing">
								<form>
									<div class="row_as">
										<input id="checkbox_billing_address" type="checkbox" name="checkbox" value="1"/>
										<label for="checkbox_billing_address">Same as shipping address</label>
									</div>
									<div class="row_as">
										<span>name</span>
										<input type="text" name="name" value="Mr. Lorem Ipsum"/>
									</div>
									<div class="row_as">
										<span>Phone</span>
										<input type="text" name="phone" value="08150985465"/>
									</div>
									<div class="row_as">
										<span>address</span>
										<textarea>Jl. Bandana Loaram No. 78 RT. 02 RW 12</textarea>
									</div>
									<div class="row_as">	
										<span>Province</span>
										<select class="select_province">
											<option value="" disabled selected>Select Province *</option>
											<option value="">option1</option>
											<option value="">option2</option>
										</select>
									</div>
									<div class="row_as">
										<span>City</span>
										<select class="select_city">
											<option value="" disabled selected>Select City *</option>
											<option value="">option1</option>
											<option value="">option2</option>
										</select>
									</div>
									<div class="row_as">
										<input type="submit" value="save changes">
										<div class="clear"></div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	
</section>
<!-- end of middle -->
<div class="clear"></div>
<?php include('inc_footer.php');?>
function hoverGs(){
	var wrapper = $('.socmed_icon');
	var wrapper_after = $('.socmed_bg');
	
	$('.socmed_icon.instagram').hover(function(){
		TweenMax.to($(".socmed_bg.instagram"),0.5,{
			css:{
				bottom:'0px',
				opacity:1,
			},
			ease:Cubic.easeOut,
		})
		
		TweenMax.to($(".socmed_icon.instagram"),0.2,{
			css:{
				background:'transparent',
			},
			ease:Cubic.easeOut,
		})

	}, function(){
		TweenMax.to($(".socmed_bg.instagram"),1.5,{
			css:{
				 bottom:'-43px',
				 opacity:0.8,
			},
			ease:Back.easeOut,
		})
		TweenMax.to($(".socmed_icon.instagram"),0.2,{
			css:{
				background:'url(images/material/instagram_soc_icon.png)no-repeat center center',  
			},
			ease:Back.easeOut,
		})
	})
	/*--*/
	$('.socmed_icon.facebook').hover(function(){
		TweenMax.to($(".socmed_bg.facebook"),0.5,{
			css:{
				bottom:'0px',
				opacity:1,
			},
			ease:Cubic.easeOut,
		})
		
		TweenMax.to($(".socmed_icon.facebook"),0.2,{
			css:{
				background:'transparent',
			},
			ease:Cubic.easeOut,
		})

	}, function(){
		TweenMax.to($(".socmed_bg.facebook"),1.5,{
			css:{
				 bottom:'-43px',
				 opacity:0.8,
			},
			ease:Back.easeOut,
		})
		TweenMax.to($(".socmed_icon.facebook"),0.2,{
			css:{
				background:'url(images/material/facebook_soc_icon.png)no-repeat center center',  
			},
			ease:Back.easeOut,
		})
	})
	/*--*/
	$('.socmed_icon.twitter').hover(function(){
		TweenMax.to($(".socmed_bg.twitter"),0.5,{
			css:{
				bottom:'0px',
				opacity:1,
			},
			ease:Cubic.easeOut,
		})
		
		TweenMax.to($(".socmed_icon.twitter"),0.2,{
			css:{
				background:'transparent',
			},
			ease:Cubic.easeOut,
		})

	}, function(){
		TweenMax.to($(".socmed_bg.twitter"),1.5,{
			css:{
				 bottom:'-43px',
				 opacity:0.8,
			},
			ease:Back.easeOut,
		})
		TweenMax.to($(".socmed_icon.twitter"),0.2,{
			css:{
				background:'url(images/material/twitter_soc_icon.png)no-repeat center center',  
			},
			ease:Back.easeOut,
		})
	})
	/*--*/
	$('.socmed_icon.message').hover(function(){
		TweenMax.to($(".socmed_bg.message"),0.5,{
			css:{
				bottom:'0px',
				opacity:1,
			},
			ease:Cubic.easeOut,
		})
		
		TweenMax.to($(".socmed_icon.message"),0.2,{
			css:{
				background:'transparent',
			},
			ease:Cubic.easeOut,
		})

	}, function(){
		TweenMax.to($(".socmed_bg.message"),1.5,{
			css:{
				 bottom:'-43px',
				 opacity:0.8,
			},
			ease:Back.easeOut,
		})
		TweenMax.to($(".socmed_icon.message"),0.2,{
			css:{
				background:'url(images/material/message_soc_icon.png)no-repeat center center',  
			},
			ease:Back.easeOut,
		})
	})
	
	/*connect*/
	$('.instagram_connect_icon').hover(function(){
		TweenMax.to($(".instagram_connect_icon_after"),0.5,{
			css:{
				bottom:'0px',
				opacity:1,
			},
			ease:Cubic.easeOut,
		})
		
		TweenMax.to($(".instagram_connect_icon"),0.2,{
			css:{
				background:'transparent',
			},
			ease:Cubic.easeOut,
		})
		
		TweenMax.to($("h5.instagram_h_label"),1.5,{
			css:{
				color:'#639c1d',
			},
			ease:Cubic.easeOut,
		})
		

	}, function(){
		TweenMax.to($(".instagram_connect_icon_after"),1.5,{
			css:{
				 bottom:'-40px',
				 opacity:0.8,
			},
			ease:Back.easeOut,
		})
		TweenMax.to($(".instagram_connect_icon"),0.2,{
			css:{
				background:'url(images/material/instagram_icon.png)no-repeat center center',  
			},
			ease:Back.easeOut,
		})
		TweenMax.to($("h5.instagram_h_label"),1,{
			css:{
				color:'#333333',
			},
			ease:Back.easeOut,
		})
	})
    
   
}
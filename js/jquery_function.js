$.fn.isOnScreen = function(){ 
    if(this.length){
        var viewport = {};
        viewport.top = $(window).scrollTop();
        viewport.bottom = viewport.top + $(window).height();
        var bounds = {};
        bounds.top = this.offset().top;
        bounds.bottom = bounds.top + this.outerHeight();
        return ((bounds.top <= viewport.bottom) && (bounds.bottom >= viewport.top));
    }else
        return false
};

function fixBannerHome(){

    var height_fixed = function(){
	 
        var elem =$(".par_after_fixed_page"),
		
		
		window_height = $(window).height();
		
		elem.css('margin-top',window_height+'px');
        
    }
    height_fixed();
    $(window).resize(function(){
        height_fixed();
    })
   
}

function fixHomePage(){
	var action = function(){
	 
        var elem =$("#middle.home"),
		
		
		window_width = $(window).width();
		window_height = $(window).height();
		
		elem.css({
            height:window_height+"px"
        });
        
    }
    action();
    $(window).resize(function(){
        action();
    })
}

$.fn.parralaxCon =function(e){
        var 
			elem = this,
			act = {},
			speed = e.speed;
		act.parallaxAction = function(){
			elem.each(function(){
				if($(this).length){
					var distance= e.distance_move;
					var top=$(window).scrollTop();
					var top2=$(this).offset().top;
					if($(this).isOnScreen()){
						top2= top2-$(window).height();
						top=top-top2; 
						var tambah = top*distance;        
				
						TweenMax.to($(this),speed,{
							css:{
								backgroundPosition:'center'+' -'+tambah+'px'
							},
							ease:Cubic.easeOut
						});
					}   
				}
			})
		}
	
    if($(window).width() > 768){
		$(window).scroll(function(){
			act.parallaxAction();
		})
		
	}
}

 


function getLoopSlide(){
	var count_slide = 0;
	$('.slider_base_seller').find("div.slide").each(function () {
		if (!$(this).hasClass('bx-clone'))
		
		count_slide++;
	});
	

//	console.log("count:" + count_slide);
}

function putDirectionSlider(){
	 var  elem = $(".wrapper_best_seller .wrapper_control_carousel a");
	 
	 elem.on('click',function(){
		var parent = $(this).parent();
		if(parent.hasClass('nav_left')){
			$(".wrapper_best_seller").attr('data-direction','prev');
		}else{
			$(".wrapper_best_seller").attr('data-direction','next');
		}

	 })

}
function getIndexActive(wrapper_slider, current_index, count_slide){
	elem = wrapper_slider,
	current_index_group = current_index + 1,
	current_min = current_index_group - 1,
	current_sum_total = current_min * 4,
	current_sum_total_group = current_sum_total + 1,
	current_count_group = current_sum_total_group + 3,
	
	attribute_elem = elem.find("div.slide[data-index=" + current_sum_total_group + "]");
	attribute_elem_text = attribute_elem.attr('current-index');
	
	$('.slider_base_seller div.slide').removeClass('fadeIn');
	
	 	
	
	 
 
	for (i = current_sum_total_group; i <= current_count_group; i++) {  
		wrapper_slider.find("div.slide[data-index=" + i + "]").addClass('fadeIn');
	} 
 
	
	// console.log("current_index:"+ current_sum_total_group + "/" + current_count_group + "total:" + count_slide + "attribute:" + attribute_elem_text);
}



function animateSlide(wrapper_slider, current_index,type){
	elem = wrapper_slider,
	current_index_group = current_index + 1,
	current_min = current_index_group - 1,
	current_sum_total = current_min * 4,
	current_sum_total_group = current_sum_total + 1,
	current_count_group = current_sum_total_group + 3,
	
	attribute_elem = elem.find("div.wrap_slide[data-index=" + current_sum_total_group + "]");
	attribute_elem_text = attribute_elem.attr('current-index');

	if(type == "next"){
		for (i = current_sum_total_group; i <= current_count_group; i++) {  
			TweenMax.set(wrapper_slider.find("div.wrap_slide[data-index=" + i + "]"),{
				opacity:0,
				left:'-30px'
			});
		} 
		var delay= 0.2;
		
		for (i = current_sum_total_group; i <= current_count_group; i++) {  
			delay= delay + 0.1;
			TweenMax.to(wrapper_slider.find("div.wrap_slide[data-index=" + i + "]"),0.3,{
				css:{
					opacity:1,
					left:'0px'			
				},
				delay:delay,
				ease:Cubic.easeOut
			});
		} 
	}else if(type == "prev"){
		for (i = current_count_group; i >= current_sum_total_group; i--) {  
			TweenMax.set(wrapper_slider.find("div.wrap_slide[data-index=" + i + "]"),{
				opacity:0,
				left:'30px'
			});
		} 
		var delay= 0.2;
		
		for (i = current_count_group; i >= current_sum_total_group; i--) {  
			delay= delay + 0.1;
			TweenMax.to(wrapper_slider.find("div.wrap_slide[data-index=" + i + "]"),0.3,{
				css:{
					opacity:1,
					left:'0px'			
				},
				delay:delay,
				ease:Cubic.easeOut
			});
		}
	}
 	
}
				


function  getHoverAttr(wrapper_slider, current_index) {
	var
		 
		elem = wrapper_slider,
		index = parseInt(current_index) + 1,
		count_slide = 0;
                
	wrapper_slider.find("div.slide").each(function () {
		if (!$(this).hasClass('bx-clone'))
		
		$("#middle.our_product .bx-wrapper .bx-prev").hover(function(){
			$(this).css({'background':'#72b422 url(images/material/'+before_elem.attr('text-hover')+')no-repeat center center'});
			}, function(){
			$(this).css("background", "black url(images/material/arrow_ourproduct.png)no-repeat 33px center");
		});
		
		$("#middle.our_product .bx-wrapper .bx-next").hover(function(){
			$(this).css({'background':'#72b422 url(images/material/'+next_elem.attr('text-hover')+')no-repeat center center'});
			}, function(){
			$(this).css("background", "black url(images/material/arrow_ourproduct_next.png)no-repeat 39px center");
		});
		
		count_slide++;
	});
	
	 
	before_index = ((index !== 1) ? (index - 1) : count_slide);
	next_index = ((index !== count_slide) ? (index + 1) : 1);
	before_elem = elem.find("div.slide[data-index=" + before_index + "]");
	next_elem = elem.find("div.slide[data-index=" + next_index + "]");
			
	}
 
 function subscribeNewsLetter(){
	$('.subscribe_newsletter').click(function(){
		TweenMax.to($(".subscribe_newsletter_form"),0.3,{
			css:{
				bottom:'0px',
			},
			ease:Quart.easeOut,
		});
		TweenMax.to($(".wrap_box_newsletter"),2,{
			css:{
				top:'70px',
				delay:2
			},
			ease:Back.easeOut,
			
		});
		
	}); 
	$('.close_icon_nav').click(function(){
		TweenMax.to($(".subscribe_newsletter_form"),1,{
			css:{
				bottom:'-370px',
				delay:2
			},
			ease:Expo.easeOut,
		});
		TweenMax.to($(".wrap_box_newsletter"),0.5,{
			css:{
				top:'20px',
			},
			ease:Expo.easeOut,
			
		});
	});
 }
 function arrowScroll(){
 
	TweenMax.to($('.scroll_down_icon'), 0.7, {css:{bottom:70}, repeat:-1, yoyo:true} ); 
}

function checkedForm(){
	$('#checkbox_reg').change(function(){
		if($(this).is(":checked")){
			//$(this).val('1');
		}else{
			//$(this).val('');
		}		 
	});
	$('#checkbox_signin').change(function(){
		if($(this).is(":checked")){
			//$(this).val('2');
		}else{
			//$(this).val('');
		}		 
	});
	$('#checkbox_guest').change(function(){
		if($(this).is(":checked")){
			$('.label_guest_check').addClass('active');
		}else{
			$('.label_guest_check').removeClass('active');
		}		 
	});
	$('#checkbox_gift_message').change(function(){
		if($(this).is(":checked")){
			$('.toggle_gift_message').slideDown(300);
		}else{
			$('.toggle_gift_message').slideUp(300);
		}		 
	});
	$('.radio_pm').change(function(){
		if($(".accordion_payment").is(":visible")){
			$('.accordion_payment').slideUp(300);
		}
		if($(this).is(":checked")){
			$("."+$(this).attr('data-accordion')).slideDown(300);
		}
	});
	
}

function popupRegister(){
	$('#pop_signin').click(function(){
		$('.overlay_popup').fadeIn(500);
	});
	
	$('.close_pop').click(function(){
		$('.overlay_popup').fadeOut(600);
	});
}

function subscribe_h(){
	$('.subscribe_newsletter .wrap_s_newsletter').hover(function(){
		TweenMax.to($(".subscribe_newsletter .before_h"),0.7,{
			css:{
				top:'0px',
			},
			ease:Back.easeOut,
		})
		TweenMax.to($(".subscribe_newsletter .after_h"),0.7,{
			css:{
				top:'0px',
			},
			ease:Back.easeOut,
		})
	}, function(){
		TweenMax.to($(".subscribe_newsletter .before_h"),0.7,{
			css:{
				 top:'-60px',
			},
			ease:Back.easeOut,
		})
		TweenMax.to($(".subscribe_newsletter .after_h"),0.7,{
			css:{
				top:'-60px',
			},
			ease:Back.easeOut,
		})
		 
	})
}

function globalEffect(){
	$(".wrap_form_newsletter input").focus(function(){
		$(this).css('border-color','#72b422');
	});
	$(".wrap_form_newsletter input").blur(function(){
		$(this).css('border-color','#666666');
	});
}

$.fn.styledSelect = function (options) {
    var isFF2 = false;
    var prefs = {
        coverClass: 'select-replace-cover',
        innerClass: 'select-replace',
        adjustPosition: {
            top: 0,
            left: 0
        },
        selectOpacity: 0
    };
    if (options)
        $.extend(prefs, options);
    return this.each(function () {
        if (isFF2)
            return false;
        var selElm = $(this);
        if (!selElm.next('span.' + prefs.innerClass).length) {
            selElm.wrap('<span><' + '/span>');
            selElm.after('<span><' + '/span>');
            var selReplace = selElm.next();
            var selCover = selElm.parent();
            selElm.css({
                'opacity': prefs.selectOpacity,
                'visibility': 'visible',
                'position': 'absolute',
                'top': 0,
                'left': 0,
                'display': 'inline',
                'z-index': 1
            });
            selCover.addClass(prefs.coverClass).css({
                'display': 'inline-block',
                'position': 'relative',
                'top': prefs.adjustPosition.top,
                'left': prefs.adjustPosition.left,
                'z-index': 0,
                'vertical-align': 'middle',
                'text-align': 'left'
            });
            selReplace.addClass(prefs.innerClass).css({
                'display': 'block',
                'white-space': 'nowrap'
            });

            selElm.bind('change', function () {
                $(this).next().text(this.options[this.selectedIndex].text);
            }).bind('resize', function () {
                $(this).parent().width($(this).width() + 'px');
            });
            selElm.trigger('change').trigger('resize');
			
        } else {
            var selElm = $(this);
            var selReplace = selElm.next();
            var selCover = selElm.parent();
            selElm.css({
                'opacity': prefs.selectOpacity,
                'visibility': 'visible',
                'position': 'absolute',
                'top': 0,
                'left': 0,
                'display': 'inline',
                'z-index': 1
            });
            selCover.css({
                'display': 'inline-block',
                'position': 'relative',
                'top': prefs.adjustPosition.top,
                'left': prefs.adjustPosition.left,
                'z-index': 0,
                'vertical-align': 'middle',
                'text-align': 'left'
            });
            selReplace.css({
                'display': 'block',
                'white-space': 'nowrap'
            });

        }
    });
};



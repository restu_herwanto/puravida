$(document).ready(function() {
	fixBannerHome();    
	getLoopSlide();    
	subscribeNewsLetter();
	arrowScroll();
	checkedForm();
	popupRegister();
	hoverGs();
	subscribe_h();
	globalEffect();
	
	$(function() {
		$("#datepicker").datepicker();
		$("#datepicker_until").datepicker();
		$("#datepicker_period").datepicker();
	});
	 
	$(function() {
		
		$('.td_qty_val').on('click', '.min', function () {
			var $quantity = $(this).siblings('.quantity_val'),
				value     = +$quantity.val();
		
			if (value > 1) {
				$quantity.val(value - 1);
			}
		});
	  
		$('.td_qty_val').on('click', '.plus', function () {
			var $quantity = $(this).siblings('.quantity_val'),
				value     = +$quantity.val();
			
			if (value < 10) {
				$quantity.val(value + 1);
			}
	  });
	});

	$("select.serving_size").styledSelect({//init custom combo
        coverClass: "combo serving_size",
        innerClass: "inner_combo",
        disableClass: 'disabled'
    });
	$("select.qty_val").styledSelect({//init custom combo
        coverClass: "combo qty_val",
        innerClass: "inner_combo",
        disableClass: 'disabled'
    });
	$("select.select_province").styledSelect({//init custom combo
        coverClass: "combo select_province",
        innerClass: "inner_combo",
        disableClass: 'disabled'
    });
	$("select.select_city").styledSelect({//init custom combo
        coverClass: "combo select_city",
        innerClass: "inner_combo",
        disableClass: 'disabled'
    });
	
	 
	
	$(".parralax_plug").parralaxCon({
        distance_move:0.2,
        speed:0.5
    }); 
	
	$('.bxslider').bxSlider({
		mode: 'fade',
		auto: 'true',
		 speed: 1000,
		easing: 'linear',

	});

	$('.slider_noche').bxSlider({
		slideWidth: 300,
		minSlides: 2,
		maxSlides: 4,          
		startSlide: 2,
		slideMargin: 20,
		responsive:'true',
		nextSelector: '#slider-prev-noche',
		prevSelector: '#slider-next-noche',
		nextText: '>',
		prevText: '<',
		onSlideBefore: function ($slideElement, oldIndex, newIndex){
			//$('#slide-counter .current-index').text(newIndex + 1);
			animateSlide($(".slider_noche"), newIndex ,' ');
		},
		onSlideNext: function($slideElement, oldIndex, newIndex){
			// getIndexActive_delay($(".slider_base_seller"), newIndex);
			animateSlide($(".slider_noche"), newIndex,'next');
		},
		onSlidePrev: function($slideElement, oldIndex, newIndex){
			// getIndexActive_delay($(".slider_base_seller"), newIndex);
			animateSlide($(".slider_noche"), newIndex ,'prev');
		}
	});
			

	
	//$('#slide-counter').prepend('<strong class="current-index"></strong>/');
	
	var slider_bs = $('.slider_base_seller').bxSlider({
		slideWidth: 300,
		minSlides: 2,
		maxSlides: 4,          
		startSlide: 2,
		slideMargin: 20,
		responsive:'true',
		nextSelector: '#slider-prev',
		prevSelector: '#slider-next',
		nextText: '>',
		prevText: '<',
		onSlideBefore: function ($slideElement, oldIndex, newIndex){
			//$('#slide-counter .current-index').text(newIndex + 1);
			animateSlide($(".slider_base_seller"), newIndex ,' ');
		},
		onSlideNext: function($slideElement, oldIndex, newIndex){
			// getIndexActive_delay($(".slider_base_seller"), newIndex);
			animateSlide($(".slider_base_seller"), newIndex,'next');
		},
		onSlidePrev: function($slideElement, oldIndex, newIndex){
			// getIndexActive_delay($(".slider_base_seller"), newIndex);
			animateSlide($(".slider_base_seller"), newIndex ,'prev');
		}
		
	});
	putDirectionSlider()//put direction data
 
	// $('#slide-counter').append(slider_bs.getSlideCount());
	
	var slider_bs = $('.slider_recommended_items').bxSlider({
		slideWidth: 300,
		minSlides: 2,
		maxSlides: 4,          
		startSlide: 2,
		slideMargin: 20,
		responsive:'true',
		nextSelector: '#slider-prev',
		prevSelector: '#slider-next',
		nextText: '>',
		prevText: '<',
		onSlideBefore: function ($slideElement, oldIndex, newIndex){
			animateSlide($(".slider_recommended_items"), newIndex ,' ');
		},
		onSlideNext: function($slideElement, oldIndex, newIndex){
			animateSlide($(".slider_recommended_items"), newIndex,'next');
		},
		onSlidePrev: function($slideElement, oldIndex, newIndex){
			animateSlide($(".slider_recommended_items"), newIndex ,'prev');
		}
		
	});
	
	var slider = $(".bxslider_ourproducts").bxSlider({
			slideWidth: "940",
         /*   onSliderLoad: function (elem) {
        getHoverAttr($(".bxslider_ourproducts"), elem, slider);
        },
            onSlideAfter: function (elem, oldIndex, newIndex) {
            getHoverAttr($(".bxslider_ourproducts"), newIndex, slider);
        },*/
		
		
    });
	
	var height_fixed = $(window).height();
	$(".par_after_fixed_page").css('margin-top',height_fixed+'px');
	
	var height_fixed_min = height_fixed - 200;
	
	var nav_icon_signin = $('.header .sign_in .img_signin');
	var nav_icon_cart = $('.header .cart_home .img_cart_home');
	
	var nav_label_cart = $('.header .cart_home .label');
	var nav_label_signin = $('.header .sign_in .label');
	
	var nav_menu = $('.br_drop_menu');
		
		if ($(window).width() > 1024) {
		
			$(window).scroll(function () {
				if ($(this).scrollTop() > 100) {
					nav_menu.addClass("br_drop_menu_act");
					nav_icon_signin.addClass("img_signin_g");
					nav_icon_cart.addClass("img_cart_home_g");
					
					nav_label_cart.addClass("label_g");
					nav_label_signin.addClass("label_g");
					
				} else {
					nav_menu.removeClass("br_drop_menu_act");
					nav_icon_signin.removeClass("img_signin_g");
					nav_icon_cart.removeClass("img_cart_home_g");
					
					nav_label_cart.removeClass("label_g");
					nav_label_signin.removeClass("label_g"); 
				}
			});
			
			
		}
		
	$(window).scroll(function () {
		
		if ($(this).scrollTop() > 100) {
				var scrollTop = $(window).scrollTop();
				scrollTop_parralax = scrollTop/5;	
				TweenMax.to($(".par_fixed_page"),0.2,{
                    css:{
					//	top:-scrollTop_parralax,
						//opacity:0.7,
                    },
					
                }); 
		}else{
            $(".par_fixed_page").css({
			//	top:'0px',
            })
			 
        }
		
		
		
		if ($(this).scrollTop() >= 50) {
			$('.menuhide_drop.active').fadeOut('slow');
		}
		if ($(this).scrollTop() < 50) {
			$('.menuhide_drop.active').fadeIn('slow');
		}
		
		var height_fixed = $(window).height();
		$(".par_after_fixed_page").css('margin-top',height_fixed+'px');
		
	});
	
	
	
});
		
 
 
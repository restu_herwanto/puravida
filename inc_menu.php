<!-- menu -->
 
<nav class="menu menuhide_drop <?php echo ($page == "home") ? "active" : "" ?>">
	<ul class="menu_left left">
		<li class="<?php echo ($page == "home") ? "active" : "" ?> anime_rh fadeIn" data-anime_rh-delay="0.1s"><a class="<?php echo ($page == "home") ? "active" : "" ?>" href="index.php">Shop</a><span class="line_hover"></span></li>
		<li class="<?php echo ($page == "about") ? "active" : "" ?> anime_rh fadeIn" data-anime_rh-delay="0.2s"><a class="<?php echo ($page == "about") ? "active" : "" ?>" href="#about">about</a><span class="line_hover"></span></li>
		<li class="<?php echo ($page == "faq") ? "active" : "" ?> anime_rh fadeIn" data-anime_rh-delay="0.3s"><a class="<?php echo ($page == "faq") ? "active" : "" ?>" href="faq.php">FAQ</a><span class="line_hover"></span></li>
	</ul>
	<nav class="logo left anime_rh fadeIn" data-anime_rh-delay="0.4s">
		<img src="images/material/logo.png"/>
	</nav>
	<ul class="menu_right left">
		<li class="<?php echo ($page == "thoughts") ? "active" : "" ?> anime_rh fadeIn" data-anime_rh-delay="0.5s"><a class="<?php echo ($page == "thoughts") ? "active" : "" ?>" href="thoughts.php">thoughts</a><span class="line_hover"></span></li>
		<li class="<?php echo ($page == "contact") ? "active" : "" ?> anime_rh fadeIn" data-anime_rh-delay="0.6s"><a class="<?php echo ($page == "contact") ? "active" : "" ?>" href="contact.php">contact</a><span class="line_hover"></span></li>
	</ul>
	<nav class="search_wrapper left anime_rh fadeIn" data-anime_rh-delay="0.7s">
		<img src="images/material/search_icon.png"/>
	</nav>
</nav>

<nav class="br_drop_menu <?php echo ($page == "home") ? "active" : "" ?>">
<nav class="menu">
	<ul class="menu_left left">
		<li class="<?php echo ($page == "home") ? "active" : "" ?>"><a class="<?php echo ($page == "home") ? "active" : "" ?>" href="index.php">Shop</a><span class="line_hover"></span></li>
		<li class="<?php echo ($page == "about") ? "active" : "" ?>"><a class="<?php echo ($page == "about") ? "active" : "" ?>" href="about.php">about</a><span class="line_hover"></span></li>
		<li class="<?php echo ($page == "faq") ? "active" : "" ?>"><a class="<?php echo ($page == "faq") ? "active" : "" ?>" href="faq.php">FAQ</a><span class="line_hover"></span></li>
	</ul>
	<nav class="logo left">
		<img src="images/material/logo.png"/>
	</nav>
	<ul class="menu_right left">
		<li class="<?php echo ($page == "thoughts") ? "active" : "" ?>"><a class="<?php echo ($page == "thoughts") ? "active" : "" ?>" href="thoughts.php">thoughts</a><span class="line_hover"></span></li>
		<li class="<?php echo ($page == "contact") ? "active" : "" ?>"><a class="<?php echo ($page == "contact") ? "active" : "" ?>" href="contact.php">contact</a><span class="line_hover"></span></li>
	</ul>
	<nav class="search_wrapper left">
		<img src="images/material/search_icon.png"/>
	</nav>
</nav>
</nav>

<!-- MENU CONTENT -->
<nav class="br_menu_content <?php echo ($page == "home") ? "hide" : "" ?>">
<nav class="menu">
	<ul class="menu_left left">
		<li class="<?php echo ($page == "home") ? "active" : "" ?>"><a class="<?php echo ($page == "home") ? "active" : "" ?>" href="index.php">Shop</a><span class="line_hover"></span></li>
		<li class="<?php echo ($page == "about") ? "active" : "" ?>"><a class="<?php echo ($page == "about") ? "active" : "" ?>" href="about.php">about</a><span class="line_hover"></span></li>
		<li class="<?php echo ($page == "faq") ? "active" : "" ?>"><a class="<?php echo ($page == "faq") ? "active" : "" ?>" href="faq.php">FAQ</a><span class="line_hover"></span></li>
	</ul>
	<nav class="logo left">
		<img src="images/material/logo.png"/>
	</nav>
	<ul class="menu_right left">
		<li class="<?php echo ($page == "thoughts") ? "active" : "" ?>"><a class="<?php echo ($page == "thoughts") ? "active" : "" ?>" href="thoughts.php">thoughts</a><span class="line_hover"></span></li>
		<li class="<?php echo ($page == "contact") ? "active" : "" ?>"><a class="<?php echo ($page == "contact") ? "active" : "" ?>" href="contact.php">contact</a><span class="line_hover"></span></li>
	</ul>
	<nav class="search_wrapper left">
		<img src="images/material/search_icon.png"/>
	</nav>
</nav>
</nav>
<div class="clear"></div>

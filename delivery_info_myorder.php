<?php
$page = "account";
include('inc_header.php');

?>
<div class="clear"></div>
<!-- middle -->
<section>
	<div id="middle" class="account_s">
		<div class="wrapper_content_page">	
			<div class="content account_s_w">
				<div class="account_s_l">
					<div class="sidebar_menu">
						<h4>My Account</h4>
						<ul>
							<li><a href="" class="">dashboard</a></li>
							<li><a href="" class="">Personal DetailS</a></li>
							<li><a href="" class="">Change Password</a></li>
							<li><a href="" class="">Delivery Info</a></li>
							<li><a href="" class="active">Order HISTORY</a></li>
							<li><a href="" class="">Payment Confirmation</a></li>
						</ul>
					</div>
				</div>
				<div class="account_s_r">
					<div class="account_content_r">
						<h1>My Order</h1>
						<div class="inner_content">
							<div class="inner_my_order">
								<div class="bar_mo">
									<input class="date_until" type="text" id="datepicker_until" placeholder="Until"/>
									<input class="date_period" type="text" id="datepicker_period" placeholder="Period"/>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	
</section>
<!-- end of middle -->
<div class="clear"></div>
<?php include('inc_footer.php');?>
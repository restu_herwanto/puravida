<?php
$page = "shop";
include('inc_header.php');

?>
<div class="clear"></div>
<!-- middle -->
<section>
	<div id="middle" class="checkout_s">
		<div class="wrapper_content_page">	
			<div class="content checkout_s_w">
				<h1>Checkout</h1>
				<div class="wrap_checkout">
				<div class="row">
					<ul class="menu_checkout_step">
						<li>
							<span class="lf step_1"></span>
							<span class="rh">
								<h4>Step 1</h4>
								<h3>sign in</h3>
							</span>
						</li>
						<li>
							<span class="space_dot"></span>
						</li>
						<li>
							<span class="lf step_2 active"></span>
							<span class="rh active">
								<h4>Step 2</h4>
								<h3>SHIPPING</h3>
							</span>
						</li>
						<li>
							<span class="space_dot"></span>
						</li>
						<li>
							<span class="lf step_3"></span>
							<span class="rh">
								<h4>Step 3</h4>
								<h3>payment</h3>
							</span>
						</li>
						<li>
							<span class="space_dot"></span>
						</li>
						<li>
							<span class="lf step_4"></span>
							<span class="rh">
								<h4>Step 4</h4>
								<h3>Review</h3>
							</span>
						</li>
					</ul>
				</div>
				<div class="errorSummary">
					<ul>
						<li>- Please enter your email</li>
						<li>- You must agree to our terms and conditions before continuing</li>
					</ul>
				</div>
				<div class="row form_shipping_address">
					<form>
					<div class="w_form_shipping_address">
						<div class="row_shipping_address">
						<h3 class="title_form">Shipping Address</h3>
							<div class="row">
								<div class="row_l">
									<input type="text" name="name" placeholder="Name *"/>
								</div>
								<div class="row_r">
									<input type="text" name="phone" placeholder="Phone *"/>
								</div>
							</div>
							<div class="row row_area">
								<textarea placeholder="Address *"></textarea>
							</div>
							<div class="row">
								<div class="row_l">
									<select class="select_province">
										<option value="" disabled selected>Select Province *</option>
										<option value="">option1</option>
										<option value="">option2</option>
									</select>
								</div>
								<div class="row_r">
									<select class="select_city">
										<option value="" disabled selected>Select City *</option>
										<option value="">option1</option>
										<option value="">option2</option>
									</select>
								</div>
							</div>
						</div>	
						<div class="clear"></div>
						<div class="row_shipping_method">
							<h3 class="title_form">Shipping Method</h3>
							<div class="row">
								<div class="row_l">
									<input id="radio1" type="radio" name="method" value="1" checked="checked"><label for="radio1">Expedition (TIKI)</label><span class="shipping_value">Rp. 15.000,-</span><br>
									<input id="radio2" type="radio" name="method" value="2"><label for="radio2">Courier</label>
								</div>
								<div class="row_r">
									<input id="radio3" type="radio" name="method" value="3"><label for="radio3">In store pickup Senopati</label><span class="shipping_value"></span><br>
									<input id="radio4" type="radio" name="method" value="4"><label for="radio4">In store pickup Pondok Indah</label>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="row_gift_message">
							<h3 class="title_form">Gift Message</h3>
							<div class="row">
								<input id="checkbox_gift_message" type="checkbox" name="checkbox" value="1"/>
								<label for="checkbox_gift_message">Ship as a gift and add a free gift message</label>
							</div>
							<div class="row toggle_gift_message">
								<div class="wrap_message">
									<input type="text" name="recipient_name" placeholder="Recipient’s Name"/>
									<textarea placeholder="Message"></textarea>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="row_btn_act">
							<div class="wrap_row_btn_act">
								<button>back</button>
								<input type="submit" value="Continue"/>
							</div>
						</div>
					</div>
					</form>
				</div>
				
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	
</section>
<!-- end of middle -->
<div class="clear"></div>
<?php include('inc_footer.php');?>
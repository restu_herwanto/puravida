<?php
$page = "shop";
include('inc_header.php');

?>
<div class="clear"></div>
<!-- middle -->
<section>
	<div id="middle" class="checkout_s">
		<div class="wrapper_content_page">	
			<div class="content checkout_s_w">
				<h1>Checkout</h1>
				<div class="wrap_checkout">
				<div class="row">
					<ul class="menu_checkout_step">
						<li>
							<span class="lf step_1"></span>
							<span class="rh">
								<h4>Step 1</h4>
								<h3>sign in</h3>
							</span>
						</li>
						<li>
							<span class="space_dot"></span>
						</li>
						<li>
							<span class="lf step_2"></span>
							<span class="rh">
								<h4>Step 2</h4>
								<h3>SHIPPING</h3>
							</span>
						</li>
						<li>
							<span class="space_dot"></span>
						</li>
						<li>
							<span class="lf step_3"></span>
							<span class="rh">
								<h4>Step 3</h4>
								<h3>payment</h3>
							</span>
						</li>
						<li>
							<span class="space_dot"></span>
						</li>
						<li>
							<span class="lf step_4 active"></span>
							<span class="rh active">
								<h4>Step 4</h4>
								<h3>Review</h3>
							</span>
						</li>
					</ul>
				</div>
				<div class="table_order_detail">
					<div class="table_order_detail_title">
						<span>Order Detail</span>
						<span><b>Order No </b>: 00001/ODR/02/2015</span>
						<div class="clear"></div>
					</div>
					<table class="order_detail_w">
						<!---->
						<?php
							for($a = 1; $a <=2; $a++){
						?>
						<tr class="od_header">
							<td colspan="4">
								<span class="l">Delivery Date :</span>
								<span class="r">February 13, 2015</span>
							</td>
						</tr>
						<tr class="od_title">
							<td>Product</td>
							<td>price</td>
							<td>qty</td>
							<td>total</td>
						</tr>
						
						<tr class="od_list">
							<td>
								<span class="l">
									<img src="images/content/cart_product_small.jpg"/>
								</span>
								<span class="r">
									<h4>Chunky Monkey</h4>
									<h5>Noche Overnight Oat - Medium Size</h5>
								</span>
							</td>
							<td>70.000,-</td>
							<td>1</td>
							<td>70.000,-</td>
						</tr>
						<tr class="od_list">
							<td>
								<span class="l">
									<img src="images/content/cart_product_small.jpg"/>
								</span>
								<span class="r">
									<h4>Chunky Monkey</h4>
									<h5>Noche Overnight Oat - Medium Size</h5>
								</span>
							</td>
							<td>70.000,-</td>
							<td>1</td>
							<td>70.000,-</td>
						</tr>
						
						<tr class="od_total">
							<td colspan="4">
								<span>180.000,-</span>
							</td>
						</tr>
						<?php } ?>
						
						<table class="order_detail_w">
						<tr class="od_detail1">
							<td rowspan="3"><span>Payment Method</span><span>Veritrans</span></td>
							<td rowspan="3"><span>Shipping Method</span><span>Expedition (JNE)</span></td>
							<td><span>sub Total</span></td>
							<td><span>360.000,-</span></td>
						</tr>
						<tr class="od_detail2">
							<td><span>Delivery fee</span></td>
							<td><span>15.000,-</span></td>
						</tr>
						<tr class="od_detail3">
							<td><span>Voucher</span></td>
							<td><span>(100.000,-)</span></td>
						</tr>
						<tr class="od_detail4">
							<td colspan="3"><span>Total</span></td>
							<td><span>275.000,-</span></td>
						</tr>
						</table>
					</table>
					
				</div>
				<div class="clear"></div>
					<div class="row_btn_act">
						<div class="wrap_row_btn_act">
							<button>back</button>
							<input type="submit" value="finish"/>
						</div>
					</div>
				
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	
</section>
<!-- end of middle -->
<div class="clear"></div>
<?php include('inc_footer.php');?>
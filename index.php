<?php
$page = "home";
include('inc_header.php');

?>
<div class="clear"></div>
<!-- middle -->
<section>
	<div id="middle" class="home par_fixed_page">
		<ul class="bxslider wrapper">
			<li style="background-image: url('images/slider/slide1.jpg');">
				<div class="tagline">
					<img src="images/material/tagline_icon.png"/>
					<h3>Wholesome</h3>
					<p>Superfood-Filled and Delicious Food Delivered to Your<br>Home or Office.</p>
					<div class="clear"></div>
					<a href="">Order Now</a>
				</div>
			</li>
			<li style="background-image: url('images/slider/slide2.jpg');">
				<div class="tagline">
					<img src="images/material/tagline_icon.png"/>
					<h3>LIVE PURE<br>LIVE FIT</h3>
					<p>Fuel your adventure with our high energy<br>and high protein treats.</p>
					<div class="clear"></div>
					<a href="">Explore</a>
				</div>
			</li>
			<li style="background-image: url('images/slider/slide3.jpg');">
				<div class="tagline">
					<img src="images/material/tagline_icon.png"/>
					<h3>We<br>believe</h3>
					<p>That feeding your body with wholesome ingredients is the<br>best long-term strategy for maintaining good health</p>
					<div class="clear"></div>
					<a href="">learn more</a>
				</div>
			</li>
		</ul>
		<a href="#ourproduct"><div class="scroll_down_icon"></div></a>
	</div>
	<div id="ourproduct">
	<div id="middle" class="our_product par_after_fixed_page">
		<div class="wrapper_product wrapper">
			<h2 class="title_page">Our Products</h2>
			 
			<div class="bxslider_ourproducts">
		
			  <div class="slide" text-hover="hover_slide.png" data-index="1">
					<div class="wrapper_list_ourproducts">
						<div class="ourproducts_left left">
							<img src="images/content/product1.jpg"/>
						</div>
						<div class="ourproducts_right left">
							<h3>NOCHË</h3>
							<h4>Purely Overnight Oats</h4>
							<p>NOCHË (“night” in Spanish) by PURAVÏDA Overnight Oats are rolled oats soaked overnight with creamy yogurt, almond milk and fresh toppings. Best enjoyed for breakfast, pre/post workout, or as a meal replacement. Vegan versions also available for those sensitive to dairy.</p>
							<ul class="wrapper_icon_list">
								<li class="icon_list_ourproduct">
									<div class="icon_img">
										<img src="images/material/serving_size_icon.jpg"/>
									</div>
									<div class="icon_text">
										<h5>Serving size:</h5>
										<h6>280 gr & 160 gr</h6>
									</div>
								</li>
								<li class="icon_list_ourproduct">
									<div class="icon_img">
										<img src="images/material/in_fridge_icon.jpg"/>
									</div>
									<div class="icon_text">
										<h5>In Fridge</h5>
										<h6>Exp 3-4 days</h6>
									</div>
								</li>
								<li class="icon_list_ourproduct">
									<div class="icon_img">
										<img src="images/material/in_freezer_icon.jpg"/>
									</div>
									<div class="icon_text">
										<h5>In Feezer</h5>
										<h6>Exp 2 weeks</h6>
									</div>
								</li>
							</ul>
							<a href=""><span class="link_our_product">View product details</span></a>
						</div>
					</div>
			  </div>
			  <div class="slide" text-hover="hover_slide.png" data-index="2">
					<div class="wrapper_list_ourproducts">
						<div class="ourproducts_left left">
							<img src="images/content/product1.jpg"/>
						</div>
						<div class="ourproducts_right left">
							<h3>NOCHË</h3>
							<h4>Purely Overnight Oats</h4>
							<p>NOCHË (“night” in Spanish) by PURAVÏDA Overnight Oats are rolled oats soaked overnight with creamy yogurt, almond milk and fresh toppings. Best enjoyed for breakfast, pre/post workout, or as a meal replacement. Vegan versions also available for those sensitive to dairy.</p>
							<ul class="wrapper_icon_list">
								<li class="icon_list_ourproduct">
									<div class="icon_img">
										<img src="images/material/serving_size_icon.jpg"/>
									</div>
									<div class="icon_text">
										<h5>Serving size:</h5>
										<h6>280 gr & 160 gr</h6>
									</div>
								</li>
								<li class="icon_list_ourproduct">
									<div class="icon_img">
										<img src="images/material/in_fridge_icon.jpg"/>
									</div>
									<div class="icon_text">
										<h5>In Fridge</h5>
										<h6>Exp 3-4 days</h6>
									</div>
								</li>
								<li class="icon_list_ourproduct">
									<div class="icon_img">
										<img src="images/material/in_freezer_icon.jpg"/>
									</div>
									<div class="icon_text">
										<h5>In Feezer</h5>
										<h6>Exp 2 weeks</h6>
									</div>
								</li>
							</ul>
							<a href=""><span class="link_our_product">View product details</span></a>
						</div>
					</div>
			 </div>
			 <div class="slide" text-hover="slide3" data-index="3">
					<div class="wrapper_list_ourproducts">
						<div class="ourproducts_left left">
							<img src="images/content/product1.jpg"/>
						</div>
						<div class="ourproducts_right left">
							<h3>NOCHË</h3>
							<h4>Purely Overnight Oats</h4>
							<p>NOCHË (“night” in Spanish) by PURAVÏDA Overnight Oats are rolled oats soaked overnight with creamy yogurt, almond milk and fresh toppings. Best enjoyed for breakfast, pre/post workout, or as a meal replacement. Vegan versions also available for those sensitive to dairy.</p>
							<ul class="wrapper_icon_list">
								<li class="icon_list_ourproduct">
									<div class="icon_img">
										<img src="images/material/serving_size_icon.jpg"/>
									</div>
									<div class="icon_text">
										<h5>Serving size:</h5>
										<h6>280 gr & 160 gr</h6>
									</div>
								</li>
								<li class="icon_list_ourproduct">
									<div class="icon_img">
										<img src="images/material/in_fridge_icon.jpg"/>
									</div>
									<div class="icon_text">
										<h5>In Fridge</h5>
										<h6>Exp 3-4 days</h6>
									</div>
								</li>
								<li class="icon_list_ourproduct">
									<div class="icon_img">
										<img src="images/material/in_freezer_icon.jpg"/>
									</div>
									<div class="icon_text">
										<h5>In Feezer</h5>
										<h6>Exp 2 weeks</h6>
									</div>
								</li>
							</ul>
							<a href=""><span class="link_our_product">View product details</span></a>
						</div>
					</div>
			 </div>
			 <div class="slide" text-hover="slide4" data-index="4">
					<div class="wrapper_list_ourproducts">
						<div class="ourproducts_left left">
							<img src="images/content/product1.jpg"/>
						</div>
						<div class="ourproducts_right left">
							<h3>NOCHË</h3>
							<h4>Purely Overnight Oats</h4>
							<p>NOCHË (“night” in Spanish) by PURAVÏDA Overnight Oats are rolled oats soaked overnight with creamy yogurt, almond milk and fresh toppings. Best enjoyed for breakfast, pre/post workout, or as a meal replacement. Vegan versions also available for those sensitive to dairy.</p>
							<ul class="wrapper_icon_list">
								<li class="icon_list_ourproduct">
									<div class="icon_img">
										<img src="images/material/serving_size_icon.jpg"/>
									</div>
									<div class="icon_text">
										<h5>Serving size:</h5>
										<h6>280 gr & 160 gr</h6>
									</div>
								</li>
								<li class="icon_list_ourproduct">
									<div class="icon_img">
										<img src="images/material/in_fridge_icon.jpg"/>
									</div>
									<div class="icon_text">
										<h5>In Fridge</h5>
										<h6>Exp 3-4 days</h6>
									</div>
								</li>
								<li class="icon_list_ourproduct">
									<div class="icon_img">
										<img src="images/material/in_freezer_icon.jpg"/>
									</div>
									<div class="icon_text">
										<h5>In Feezer</h5>
										<h6>Exp 2 weeks</h6>
									</div>
								</li>
							</ul>
							<a href=""><span class="link_our_product">View product details</span></a>
						</div>
					</div>
			 </div>
			</div>
			 
		</div>
	</div>
	</div>
	<div id="middle" class="par_webelieve parralax_plug">
		<div class="wrapper_par_webelieve wrapper anime_rh fadeIn">
			<h5>We Believe in Simplicity</h5>
			<h6>High quality foods, wholesome ingredients and loving hands</h6>
			<div class="wrapper_par_webelieve_list">
				<ul class="list_par_webelieve_left">
					<li><span>No artificial colors and flavors</span></li>
					<li><span>No refined sugar and hydrogenated oils</span></li>
					<li><span>No preservatives and additives</span></li>
				</ul>
				<ul class="list_par_webelieve_right">
					<li><span>Yes to fresh, nutrient-dense ingredients</span></li>
					<li><span>Yes to thoughtful sourcing</span></li>
					<li><span>Yes to delicious, unique flavors</span></li>
				</ul>
			</div>
		</div>
	</div>
	<div id="middle" class="best_seller">
		<div class="wrapper_best_seller wrapper">
			<h2 class="title_page anime_rh fadeIn">Best Sellers</h2>
			<div class="wrapper_control_carousel anime_rh fadeIn">
				<div class="nav_left" id="slider-next"></div>
				<div class="label_center">View All</div>
				<div class="nav_right" id="slider-prev"></div>
			</div>
			<div class="clear"></div>
			<div class="slider_base_seller anime_rh fadeIn">
				<div class="slide">
					<div class="wrap_slide" current-index="data1" data-index="1">
						<img src="images/content/best_seller1.jpg"/>
						<h4 class="title_slide_base">Chunky Monkey</h4>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" current-index="data2" data-index="2">
						<img src="images/content/best_seller2.jpg"/>
						<h4 class="title_slide_base">Kakao Chia</h4>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" current-index="data3" data-index="3">
						<img src="images/content/best_seller3.jpg"/>
						<h4 class="title_slide_base">Purely Granola</h4>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" current-index="data4" data-index="4">
						<img src="images/content/best_seller4.jpg"/>
						<h4 class="title_slide_base">Organic Honey</h4>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" current-index="data5" data-index="5">
						<img src="images/content/best_seller1.jpg"/>
						<h4 class="title_slide_base">Chunky Monkey</h4>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" current-index="data6" data-index="6">
						<img src="images/content/best_seller2.jpg"/>
						<h4 class="title_slide_base">Kakao Chia</h4>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" current-index="data7" data-index="7">
						<img src="images/content/best_seller3.jpg"/>
						<h4 class="title_slide_base">Purely Granola</h4>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" current-index="data8" data-index="8">
						<img src="images/content/best_seller4.jpg"/>
						<h4 class="title_slide_base">Organic Honey</h4>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" current-index="data9" data-index="9">
						<img src="images/content/best_seller1.jpg"/>
						<h4 class="title_slide_base">Chunky Monkey</h4>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" current-index="data10" data-index="10">
						<img src="images/content/best_seller2.jpg"/>
						<h4 class="title_slide_base">Kakao Chia</h4>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" current-index="data11" data-index="11">
						<img src="images/content/best_seller3.jpg"/>
						<h4 class="title_slide_base">Purely Granola</h4>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" current-index="data12" data-index="12">
						<img src="images/content/best_seller4.jpg"/>
						<h4 class="title_slide_base">Organic Honey</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="middle" class="noche">
		<div class="wrapper_noche wrapper">
			<h2 class="title_page anime_rh fadeIn">NOCHË Overnight Oats</h2>
			<div class="wrapper_control_carousel anime_rh fadeIn">
				<div class="nav_left" id="slider-next-noche"></div>
				<div class="label_center">View All</div>
				<div class="nav_right" id="slider-prev-noche"></div>
			</div>
			<div class="clear"></div>
			<div class="slider_noche anime_rh fadeIn">
				<div class="slide">
					<div class="wrap_slide" data-index="1">
						<img src="images/content/noche1.jpg"/>
						<h4 class="title_slide_base">Strawberry Cheese Cake</h4>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" data-index="2">
						<img src="images/content/noche2.jpg"/>
						<h4 class="title_slide_base">Sticky Date</h4>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" data-index="3">
						<img src="images/content/noche3.jpg"/>
						<h4 class="title_slide_base">Chia Nilla</h4>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" data-index="4">
						<img src="images/content/noche4.jpg"/>
						<h4 class="title_slide_base">Mat Chia</h4>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" data-index="5">
						<img src="images/content/noche1.jpg"/>
						<h4 class="title_slide_base">Strawberry Cheese Cake</h4>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" data-index="6">
						<img src="images/content/noche2.jpg"/>
						<h4 class="title_slide_base">Sticky Date</h4>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" data-index="7">
						<img src="images/content/noche3.jpg"/>
						<h4 class="title_slide_base">Chia Nilla</h4>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" data-index="8">
						<img src="images/content/noche4.jpg"/>
						<h4 class="title_slide_base">Mat Chia</h4>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" data-index="9">
						<img src="images/content/noche1.jpg"/>
						<h4 class="title_slide_base">Strawberry Cheese Cake</h4>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" data-index="10">
						<img src="images/content/noche2.jpg"/>
						<h4 class="title_slide_base">Sticky Date</h4>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" data-index="11">
						<img src="images/content/noche3.jpg"/>
						<h4 class="title_slide_base">Chia Nilla</h4>
					</div>
				</div>
				<div class="slide">
					<div class="wrap_slide" data-index="12">
						<img src="images/content/noche4.jpg"/>
						<h4 class="title_slide_base">Mat Chia</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="middle" class="par_tagline parralax_plug">
		<div class="wrapper_par_tagline wrapper anime_rh fadeIn">
			<h5 class="tagline_h5">The body is your temple, keep it pure and clean<br>for the soul to reside in.</h5>
			<h6 class="tagline_h6">~ BKS Iyengar</h6>
		</div>
	</div>
	<div id="middle" class="grid_home">
		<div class="wrapper">
			<div class="wrapper_instagram">
				<a href="">
				<span class="instagram_connect_icon" style="background: url('images/material/instagram_icon.png');">
					<span class="instagram_connect_icon_after" style="background: url('images/material/instagram_icon_hover.png');"></span>
				</span>
				</a>
				<h5 class="instagram_h_label">#puravidajkt</h5>
			</div>
			<div class="grid_home_wrapper">
				<div class="grid_width_wrapper">
				
					<div class="grid_small left">
						<div class="grid_img">
							<figure class="effect-oscar">
							<img src="images/content/ins_image1.jpg"/>
								<figcaption>
									<p>Lorem Ipsum</p>
								</figcaption>	
							</figure>
						</div>
						<div class="grid_img">
							<figure class="effect-oscar">
							<img src="images/content/ins_image2.jpg"/>
								<figcaption>
									<p>Lorem Ipsum</p>
								</figcaption>	
							</figure>
						</div>
					</div>
					
					<div class="grid_large left">
						<div class="grid_img">
							<figure class="effect-oscar">
							<img src="images/content/ins_image3.jpg"/>
								<figcaption>
									<p>Lorem Ipsum</p>
								</figcaption>	
							</figure>
						</div>
					</div>
					
					<div class="grid_large left">
						<div class="grid_img">
							<figure class="effect-oscar">
							<img src="images/content/ins_image4.jpg"/>
								<figcaption>
									<p>Lorem Ipsum</p>
								</figcaption>	
							</figure>
						</div>
					</div>
					
					<div class="grid_small left">
						<div class="grid_img">
							<figure class="effect-oscar">
							<img src="images/content/ins_image5.jpg"/>
								<figcaption>
									<p>Lorem Ipsum</p>
								</figcaption>	
							</figure>
						</div>
						<div class="grid_img">
							<figure class="effect-oscar">
							<img src="images/content/ins_image6.jpg"/>
								<figcaption>
									<p>Lorem Ipsum</p>
								</figcaption>	
							</figure>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	<div class="clear"></div>
	<div id="middle" class="connect_socmed">
		<div class="wrapper_connect_socmed wrapper">
			<h5 class="socmed">Connect with Us</h5>
			<ul>
				<li>
					<a href="">
						<span class="socmed_icon instagram" style="background: url('images/material/instagram_soc_icon.png');">
							<span class="socmed_bg instagram" style="background: url('images/material/instagram_soc_icon_hover.png');"></span>
						</span>
						<p class="text_socmed">@puravidajkt</p>
					</a>
				</li>
				<li>
					<a href="">
						<span class="socmed_icon facebook" style="background: url('images/material/facebook_soc_icon.png');">
							<span class="socmed_bg facebook" style="background: url('images/material/facebook_soc_icon_hover.png');"></span>
						</span>
						<p class="text_socmed">puravidajkt</p>
					</a>
				</li>
				<li>
					<a href="">
						<span class="socmed_icon twitter" style="background: url('images/material/twitter_soc_icon.png');">
							<span class="socmed_bg twitter" style="background: url('images/material/twitter_soc_icon_hover.png');"></span>
						</span>
						<p class="text_socmed">@puravidajkt</p>
					</a>
				</li>
				<li>
					<a href="">
						<span class="socmed_icon message" style="background: url('images/material/message_soc_icon.png');">
							<span class="socmed_bg message" style="background: url('images/material/message_soc_icon_hover.png');"></span>
						</span>
						<p class="text_socmed">puravidajkt@gmail.com</p>
					</a>
				</li>
			</ul>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</section><!-- end of middle -->
<div class="clear"></div>
<?php include('inc_footer.php');?>